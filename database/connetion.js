const { Sequelize } = require("sequelize")

const sequelize = new Sequelize('modeling', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql'
});

try {
    sequelize.authenticate();
    console.log('connection successful')
} catch (error) {
    console.log('connection abort dur to ' + error.message)
}



module.exports = sequelize