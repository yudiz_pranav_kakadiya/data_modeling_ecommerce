'use strict';

module.exports = {
  async up(queryInterface, DataTypes) {
    await queryInterface.createTable('product',
      {
        id: {
          type: DataTypes.BIGINT,
          autoIncrement: true,
          primaryKey: true
        },
        sProductName: {
          type: DataTypes.STRING,
          allowNull: false
        },
        sDiscription: {
          type: DataTypes.STRING,
          allowNull: false
        },
        nPrice: {
          type: DataTypes.BIGINT,
          allowNull: false
        },
        nStock: {
          type: DataTypes.BIGINT,
          allowNull: false
        },
        iSellerId: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
            model: "sellerRegister",
            key: 'id'
          },
        },
        iCategoryId: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
            model: "category",
            key: 'id'
          },
        },
        eStatus: {
          type: DataTypes.ENUM("in stocks", "out of stocks"),
          defaultValue: "in stocks"
        },
        dCreatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        dUpdatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      }

    )
  },

  async down(queryInterface) {
    await queryInterface.dropTable('product');
  }
};
