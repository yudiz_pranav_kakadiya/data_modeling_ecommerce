'use strict';

module.exports = {
  async up(queryInterface, DataTypes) {
    await queryInterface.createTable('productfaq',
      {
        id: {
          type: DataTypes.BIGINT,
          autoIncrement: true,
          primaryKey: true
        },
        sQuestion: {
          type: DataTypes.DATE,
          allowNull: false
        },
        sAnswer: {
          type: DataTypes.DATE,
          allowNull: false
        },
        iProductId: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
            model: "product",
            key: 'id'
          },
        },
        iCustomerId: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
            model: "customer",
            key: 'id'
          },
        },
        dCreatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        dUpdatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      }
    )
  },

  async down(queryInterface) {
    await queryInterface.dropTable('productfaq');
  }
};
