'use strict';

module.exports = {
  async up(queryInterface, DataTypes) {
    await queryInterface.createTable('transaction',
      {
        id: {
          type: DataTypes.INTEGER,
          autoIncrement: true,
          primaryKey: true
        },
        eStatus: {
          type: DataTypes.ENUM("failed", "successfull"),
          default: null
        },
        sPaymentMethod: {
          type: DataTypes.STRING,
          allowNull: false
        },
        iOrderId: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
            model: "order",
            key: 'id'
          },
        },
        iSellerId: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
            model: "sellerRegister",
            key: 'id'
          },
        },
        nTotalAmount: {
          type: DataTypes.BIGINT,
          allowNull: false
        },
        dCreatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        dUpdatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      }
    )
  },
  async down(queryInterface) {
    await queryInterface.dropTable('transaction');
  }
};
