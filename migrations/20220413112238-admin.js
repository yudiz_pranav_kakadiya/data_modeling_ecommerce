'use strict';

module.exports = {
  async up(queryInterface, DataTypes) {
    await queryInterface.createTable('admin',
      {
        id: {
          type: DataTypes.BIGINT,
          autoIncrement: true,
          primaryKey: true
        },
        sFullName: {
          type: DataTypes.STRING,
          allowNull: false
        },
        sEmail: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true
        },
        sMobile: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true,
        },
        sPassword: {
          type: DataTypes.STRING,
          allowNull: false
        },
        dCreatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        dUpdatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      }
    )
  },

  async down(queryInterface) {
    await queryInterface.dropTable('admin');
  }
};
