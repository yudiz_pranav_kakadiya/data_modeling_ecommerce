'use strict';

module.exports = {
  async up(queryInterface, DataTypes) {
    await queryInterface.createTable('order',
      {
        id: {
          type: DataTypes.BIGINT,
          autoIncrement: true,
          primaryKey: true
        },
        nRating: {
          type: DataTypes.BIGINT,
          allowNull: false
        },
        iProductId: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
            model: "product",
            key: 'id'
          },
        },
        sDescription: {
          type: DataTypes.STRING,
          allowNull: false
        },
        dCreatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        dUpdatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      }
    )
  },

  async down(queryInterface) {
    await queryInterface.dropTable('order');
  }
};
