'use strict';

module.exports = {
  async up(queryInterface, DataTypes) {
    await queryInterface.createTable('address',
      {
        id: {
          type: DataTypes.BIGINT,
          autoIncrement: true,
          primaryKey: true
        },
        eAddressType: {
          type: DataTypes.ENUM("WORK", "HOME", "OFFICE"),
          defaultValue: "HOME"
        },
        sFullName: {
          type: DataTypes.STRING,
          allowNull: false
        },
        sDistrict: {
          type: DataTypes.STRING,
          allowNull: false
        },
        sCity: {
          type: DataTypes.STRING,
          allowNull: false
        },
        sCountry: {
          type: DataTypes.STRING,
          allowNull: false
        },
        nPincode: {
          type: DataTypes.INTEGER,
          allowNull: false
        },
        iCustomerId: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
            model: "customer",
            key: 'id'
          },
        },
        dCreatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        dUpdatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      }
    )
  },

  async down(queryInterface) {
    await queryInterface.dropTable('address');
  }
};
