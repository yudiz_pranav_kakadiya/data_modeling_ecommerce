'use strict';

module.exports = {
  async up(queryInterface, DataTypes) {
    await queryInterface.createTable('category',
      {
        id: {
          type: DataTypes.BIGINT,
          autoIncrement: true,
          primaryKey: true
        },
        sCategoryName: {
          type: DataTypes.STRING,
          allowNull: false
        },
        sDiscription: {
          type: DataTypes.STRING,
          allowNull: false
        },
        dCreatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        dUpdatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      }

    )
  },

  async down(queryInterface) {
    await queryInterface.dropTable('category');
  }
};
