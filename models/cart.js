'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Cart extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Cart.init({
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        nQunatity: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 1
        },
        nTotalAmout: {
            type: DataTypes.BIGINT,
            allowNull: false,
        },
        iProductId: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: "product",
                key: 'id'
            },
        },
        dCreatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        dUpdatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'cart',
    });
    return Cart;
};