'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Review extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Review.init({
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        nRating: {
            type: DataTypes.BIGINT,
            allowNull: false
        },
        iProductId: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: "product",
                key: 'id'
            },
        },
        sDescription: {
            type: DataTypes.STRING,
            allowNull: false
        },
        dCreatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        },

        dUpdatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'review',
    });
    return Review;
};