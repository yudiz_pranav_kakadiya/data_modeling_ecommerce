'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Order extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Order.init({
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        iProductId: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: "product",
                key: 'id'
            },
        },
        iCustomerId: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: "customer",
                key: 'id'
            },
        },
        sFullName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nQunatity: {
            type: DataTypes.BIGINT,
            allowNull: false,
        },
        nAmount: {
            type: DataTypes.BIGINT,
            allowNull: false,
        },
        eStatus: {
            type: DataTypes.ENUM("pending", "delivered", "shipped"),
            allowNull: "pending",
        },
        dCreatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        dUpdatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        dActivityDate: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'order',
    });
    return Order;
};