'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Invoice extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Invoice.init({
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        iTransactionId: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: "transaction",
                key: 'id'
            },
        },
        iSellerId: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: "sellerRegister",
                key: 'id'
            },
        },
        dCreatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        dUpdatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        }
    },
        {
            sequelize,
            modelName: 'invoice',
        });
    return Invoice;
};