'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Product extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Product.init({
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        sProductName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        sDiscription: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nPrice: {
            type: DataTypes.BIGINT,
            allowNull: false
        },
        nStock: {
            type: DataTypes.BIGINT,
            allowNull: false
        },
        iSellerId: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: "sellerRegister",
                key: 'id'
            },
        },
        iCategoryId: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: "category",
                key: 'id'
            },
        },
        eStatus: {
            type: DataTypes.ENUM("in stocks", "out of stocks"),
            defaultValue: "in stocks"
        },
        dCreatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        dUpdatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'product',
    });
    return Product;
};