'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Customer extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Customer.init({
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        sFullName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        sEmail: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        sMobile: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },
        bIsActive: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        sPassword: {
            type: DataTypes.STRING,
            allowNull: false
        },
        dCreatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        dUpdatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'customer',
    });
    return Customer;
};