'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Address extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Address.init({
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        eAddressType: {
            type: DataTypes.ENUM("WORK", "HOME", "OFFICE"),
            defaultValue: "HOME"
        },
        sFullName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        sDistrict: {
            type: DataTypes.STRING,
            allowNull: false
        },
        sCity: {
            type: DataTypes.STRING,
            allowNull: false
        },
        sCountry: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nPincode: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        iCustomerId: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: "customer",
                key: 'id'
            },
        },
        dCreatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        dUpdatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'address',
        indexes: [{ unique: true, fields: ['iCustomerId'] }]
    });
    return Address;
};