'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class ProductFaq extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    ProductFaq.init({
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        sQuestion: {
            type: DataTypes.DATE,
            allowNull: false
        },
        sAnswer: {
            type: DataTypes.DATE,
            allowNull: false
        },
        iProductId: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: "product",
                key: 'id'
            },
        },
        iCustomerId: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: "customer",
                key: 'id'
            },
        },
        dCreatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        dUpdatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'productfaq',
    });
    return ProductFaq;
};