'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Transaction extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Transaction.init({
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        eStatus: {
            type: DataTypes.ENUM("failed", "successfull"),
            default: null
        },
        sPaymentMethod: {
            type: DataTypes.STRING,
            allowNull: false
        },
        iOrderId: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: "order",
                key: 'id'
            },
        },
        iSellerId: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: "sellerRegister",
                key: 'id'
            },
        },
        nTotalAmount: {
            type: DataTypes.BIGINT,
            allowNull: false
        },
        dCreatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        dUpdatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'transaction',
    });
    return Transaction;
};